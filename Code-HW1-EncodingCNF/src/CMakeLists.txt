file(GLOB SUBDIRS "${CMAKE_CURRENT_SOURCE_DIR}/*")
foreach(SUBDIR ${SUBDIRS})
    if(IS_DIRECTORY ${SUBDIR})
        get_filename_component(SUBDIR_NAME ${SUBDIR} NAME)
        add_subdirectory(${SUBDIR_NAME})
    endif(IS_DIRECTORY ${SUBDIR})
endforeach(SUBDIR ${SUBDIRS})