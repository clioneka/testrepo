#include <iostream>
#include <fstream>
#include <string>


int main(int argc, char** argv) {

    if (argc != 4) {
        std::cout << "Wrong Parameter Number!" << std::endl;
        std::cin.get();
        return -1;
    }
    for (size_t i = 0; i < argc; i++) {
        std::cout << "argv[" << i << "] = " << argv[i] << std::endl;
    }

    // 開啟輸入檔案
    std::ifstream ifs;
    ifs.open(argv[1]);

    std::string str;
    std::getline(ifs, str, '\n');
    std::cout << "str = " << str << std::endl;

    // std::string cinstr;
    // std::getline(std::cin, cinstr, '\n');
    // std::cout << cinstr << std::endl;

    ifs.close();
    std::cin.get();
    return 0;
}