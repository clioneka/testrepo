/*
./solver sudoku_9x9.txt 
*/

#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <limits.h>


using namespace std;

int PRESET_FALSE = INT_MIN;
int PRESET_TRUE = INT_MAX;


int PUZZLE_SIZE;
int INIT_SET;


vector< vector<int> > Create_Puzzle(char* input_file){

    vector< vector<int> > puzzle;
    
    // 開啟輸入檔案
    ifstream ifs;
    ifs.open(input_file);
            
    int n = -1;
    while (1) {
        string s, num_str;
        int num;
        getline(ifs, s, '\n');          // 讀取
        s.push_back('\n');

        vector<int> puzzle_line;

        for (int i = 0; i < (int)s.length(); i++) {

            if (s[i] == '\n') {
                sscanf(num_str.c_str(), "%d", &num);
                puzzle_line.push_back(num);
                num_str.clear();
                break;
            } else if (s[i] == ' ') {
                sscanf(num_str.c_str(), "%d", &num);
                puzzle_line.push_back(num);
                num_str.clear();
            } else {
                num_str.push_back(s[i]);
            }
        }

        puzzle.push_back(puzzle_line);
        if(n == -1)  n = (int)puzzle_line.size() - 1;
        else         n--;
        if(n == 0)   break;
    }

    ifs.close();
    return puzzle;
}


int main(int argc, char** argv){

    vector< vector<int> > puzzle;
    puzzle = Create_Puzzle(argv[1]);
    PUZZLE_SIZE = (int)puzzle.size();
    int PUZZLE_SIZE_SQRT = (int)sqrt(PUZZLE_SIZE);

    //for(int i=0; i<PUZZLE_SIZE; i++){
    //    for(int j=0; j<PUZZLE_SIZE; j++)
    //        cout << puzzle[i][j] << " ";
    //    cout << endl;
    //}


// Encoding Implementation
    vector< vector < vector<int> > > VarNums;

    // 初始化
    for(int i=0; i<PUZZLE_SIZE; i++){
        vector<vector<int> > temp_1;
        for(int j = 0; j<PUZZLE_SIZE; j++){
            vector<int> temp_2;
            for(int k = 0; k<PUZZLE_SIZE; k++)
                temp_2.push_back(0);
            temp_1.push_back(temp_2);
        }
        VarNums.push_back(temp_1);
    }

    // 初始化已填入的格子
    for(int r = 0; r < PUZZLE_SIZE; r++) {
        for(int c = 0; c < PUZZLE_SIZE; c++){
            if(puzzle[r][c] != 0){
                int val = puzzle[r][c] - 1;
                for(int i = 0; i < PUZZLE_SIZE; i++)
                    VarNums[r][c][i] = PRESET_FALSE;
                for(int i = 0; i < PUZZLE_SIZE; i++)
                    VarNums[i][c][val] = PRESET_FALSE;   
                for(int i = 0; i < PUZZLE_SIZE; i++)
                    VarNums[r][i][val] = PRESET_FALSE;   

                int g_r = (r / PUZZLE_SIZE_SQRT)*PUZZLE_SIZE_SQRT;
                int g_c = (c / PUZZLE_SIZE_SQRT)*PUZZLE_SIZE_SQRT;
                for(int i=0; i<PUZZLE_SIZE_SQRT; i++)
                    for(int j=0; j<PUZZLE_SIZE_SQRT; j++)
                        VarNums[g_r + i][g_c + j][val] = PRESET_FALSE;

                VarNums[r][c][val] = PRESET_TRUE;
            }
        }
    }
    

    // Assign number to unfilled cell
    int var_counter = 1;
    for(size_t r = 0; r < PUZZLE_SIZE; r++)
        for(size_t c = 0; c < PUZZLE_SIZE; c++)
            for(size_t d = 0; d < PUZZLE_SIZE; d++)
                if(VarNums[r][c][d] == 0){
                    VarNums[r][c][d] = var_counter;
                    var_counter++;
                }
        

    /*
        for a digit in a row will have C(9,2)+1 clauses
        => n digit in n row : (n^2)*(C(9,2)+1)

        we have n row, n column, n block
    */

    vector<vector<int> > clauses;
    ofstream output_file;
    output_file.open(argv[2]);


    // CNF for digit
    for(int r=0; r<PUZZLE_SIZE; r++){
        for(int c=0; c<PUZZLE_SIZE; c++){

            vector<int> clause_at_least_one;
            bool breakout = false;
            for(int d=0; d<PUZZLE_SIZE; d++)
                if(VarNums[r][c][d] == PRESET_TRUE){
                    breakout = true;
                    break;
                }else if(VarNums[r][c][d] == PRESET_FALSE)
                    continue;
                else
                    clause_at_least_one.push_back((VarNums[r][c][d]));
            
            if(clause_at_least_one.empty() && breakout == false){
                //cout << "ERROR: DIGIT: conflict clause: (row, column) Digit ("<< r << ", " << c << ")" << endl;
                output_file << "NO" << endl;
                output_file.close();
                return 0;
            }else if(breakout == false){
                clauses.push_back((clause_at_least_one));
            }
        }

        for(int c=0; c<PUZZLE_SIZE; c++)
            for(int d=0; d<PUZZLE_SIZE-1; d++)
                for(int k=d+1; k<PUZZLE_SIZE; k++){
                    vector<int> clause_at_most_one;
                    if(VarNums[r][c][d] == PRESET_FALSE)
                        break;
                    else if(VarNums[r][c][d] != PRESET_TRUE)
                        clause_at_most_one.push_back(VarNums[r][c][d]*(-1));

                    if(VarNums[r][c][k] == PRESET_FALSE)
                        continue;
                    else if(VarNums[r][c][k] != PRESET_TRUE)
                        clause_at_most_one.push_back(VarNums[r][c][k]*(-1));

                    if(clause_at_most_one.empty()){
                        cout << "ERROR: Digit: 2 true" << endl;
                        exit(0);
                    }else{
                        clauses.push_back(clause_at_most_one);
                    }
                }
    }


    // CNF for row
    for(int r=0; r<PUZZLE_SIZE; r++){
        for(int d=0; d<PUZZLE_SIZE; d++){
            vector<int> clause_at_least_one;
            bool breakout = false;
            for (int c = 0; c < PUZZLE_SIZE; c++)
                if(VarNums[r][c][d] == PRESET_TRUE){
                    breakout = true;
                    break;
                }else if(VarNums[r][c][d] == PRESET_FALSE)
                    continue;
                else
                    clause_at_least_one.push_back((VarNums[r][c][d]));
            
            if(clause_at_least_one.empty() && breakout == false){
               // cout << "ERROR: ROW: conflict clause: Row "<< r << endl;
                output_file << "NO" << endl;
                output_file.close();
                return 0;
            }else if(breakout == false){
                clauses.push_back((clause_at_least_one));
            }
        }

        for(int d=0; d<PUZZLE_SIZE; d++)
            for(int c=0; c<PUZZLE_SIZE-1; c++)
                for(int k=c+1; k<PUZZLE_SIZE; k++){
                    vector<int> clause_at_most_one;
                    if(VarNums[r][c][d] == PRESET_FALSE)
                        break;
                    else if(VarNums[r][c][d] != PRESET_TRUE)
                        clause_at_most_one.push_back(VarNums[r][c][d]*(-1));

                    if(VarNums[r][k][d] == PRESET_FALSE)
                        continue;
                    else if(VarNums[r][k][d] != PRESET_TRUE)
                        clause_at_most_one.push_back(VarNums[r][k][d]*(-1));

                    if(clause_at_most_one.empty()){
                        cout << "ERROR: ROW: 2 true" << endl;
                        exit(0);
                    }else{
                        clauses.push_back(clause_at_most_one);
                    }
                }
    }


    // CNF for column
    for(int c=0; c<PUZZLE_SIZE; c++){
        for(int d=0; d<PUZZLE_SIZE; d++){
            vector<int> clause_at_least_one;
            bool breakout = false;
            for(int r=0; r<PUZZLE_SIZE; r++)
                if(VarNums[r][c][d] == PRESET_TRUE){
                    breakout = true;
                    break;
                }else if(VarNums[r][c][d] == PRESET_FALSE)
                    continue;
                else
                    clause_at_least_one.push_back((VarNums[r][c][d]));
            
            if(clause_at_least_one.empty() && breakout == false){
                //cout << "ERROR: COLUMN: conflict clause: Column "<< c << endl;
                output_file << "NO" << endl;
                output_file.close();
                return 0;
            }else if(breakout ==false){
                clauses.push_back((clause_at_least_one));
            }
        }

        for(int d=0; d<PUZZLE_SIZE; d++)
            for(int r=0; r<PUZZLE_SIZE-1; r++)
                for(int k=r+1; k<PUZZLE_SIZE; k++){
                    vector<int> clause_at_most_one;
                    if(VarNums[r][c][d] == PRESET_FALSE)
                        break;
                    else if(VarNums[r][c][d] != PRESET_TRUE)
                        clause_at_most_one.push_back(VarNums[r][c][d]*(-1));

                    if(VarNums[k][c][d] == PRESET_FALSE)
                        continue;
                    else if(VarNums[k][c][d] != PRESET_TRUE)
                        clause_at_most_one.push_back(VarNums[k][c][d]*(-1));

                    if(clause_at_most_one.empty()){
                        cout << "ERROR: COLUMN: 2 true" << endl;
                        exit(0);
                    }else{
                        clauses.push_back(clause_at_most_one);
                    }
                }
    }


    // CNF for Block
    for(int g=0; g<PUZZLE_SIZE; g++){  
        int g_r = (g / PUZZLE_SIZE_SQRT)*PUZZLE_SIZE_SQRT;
        int g_c = (g % PUZZLE_SIZE_SQRT)*PUZZLE_SIZE_SQRT;
        bool breakout = false;

        for(int d=0; d<PUZZLE_SIZE; d++){
            vector<int> clause_at_least_one;

            for(int r=0; r<PUZZLE_SIZE_SQRT; r++){
                for(int c=0; c<PUZZLE_SIZE_SQRT; c++){
                    if(VarNums[g_r+r][g_c+c][d] == PRESET_TRUE){
                        breakout = true;
                        break;
                    }else if(VarNums[g_r+r][g_c+c][d] == PRESET_FALSE)
                        continue;
                    else
                        clause_at_least_one.push_back((VarNums[g_r+r][g_c+c][d]));
                }
                if(breakout)
                    break;
            }
            
            if(clause_at_least_one.empty() && breakout == false){
                //cout << "ERROR: BLOCK: conflict clause: Block "<< g << endl;
                output_file << "NO" << endl;
                output_file.close();
                return 0;
            }else if(breakout == false){
                clauses.push_back((clause_at_least_one));
            }
        }

        for(int d=0; d<PUZZLE_SIZE; d++)
            
            for(int i=0; i<PUZZLE_SIZE-1; i++)
                for(int j=i+1; j<PUZZLE_SIZE; j++){
                    vector<int> clause_at_most_one;
                    int r1 = g_r + (i / PUZZLE_SIZE_SQRT);
                    int c1 = g_c + (i % PUZZLE_SIZE_SQRT);

                    int r2 = g_r + (j / PUZZLE_SIZE_SQRT);
                    int c2 = g_c + (j % PUZZLE_SIZE_SQRT);

                    if(VarNums[r1][c1][d] == PRESET_FALSE)
                        break;
                    else if(VarNums[r1][c1][d] != PRESET_TRUE)
                        clause_at_most_one.push_back(VarNums[r1][c1][d]*(-1));

                    if(VarNums[r2][c2][d] == PRESET_FALSE)
                        continue;
                    else if(VarNums[r2][c2][d] != PRESET_TRUE)
                        clause_at_most_one.push_back(VarNums[r2][c2][d]*(-1));

                    if(clause_at_most_one.empty()){
                        cout << "ERROR: BLOCK: 2 true" << endl;
                        exit(0);
                    }else{
                        clauses.push_back(clause_at_most_one);
                    }
                }
    }

    int clause_num = (int)clauses.size();
    int var_num = var_counter;


    // 輸出到 MiniSAT    
    ofstream ofs;
    ofs.open("Encoding_File.cnf");
    ofs << "p cnf " << var_num << " " << clause_num << endl;

    for(int i=0; i<clause_num; i++){
        for(int j=0; j<clauses[i].size(); j++)
            ofs << clauses[i][j] << " ";
        ofs << "0" << endl;
    }
    ofs.close();
    
    string command = "./" + string(argv[3]) + "Encoding_File.cnf MiniSat_Result.txt";
    int result = system("./MiniSat_v1.14_linux Encoding_File.cnf MiniSat_Result.txt");
    
    ifstream ifs;
    ifs.open("MiniSat_Result.txt");

    int ans;
    string Solvable;
    ifs >> Solvable;
    if (Solvable == "SAT") {
        ifs >> ans;
        for (int r=0; r<PUZZLE_SIZE; r++)
            for (int c=0; c<PUZZLE_SIZE; c++)
                for (int d=0; d<PUZZLE_SIZE; d++){
                    if (VarNums[r][c][d] == abs(ans)){
                        if (ans > 0)
                            VarNums[r][c][d] = PRESET_TRUE;
                        else 
                            VarNums[r][c][d] = PRESET_FALSE;
                        ifs >> ans;
                    }
                }
    } else {
        output_file << "NO" << endl;
    }

    ifs.close();

    for(int r=0; r<PUZZLE_SIZE; r++){
        for(int c=0; c<PUZZLE_SIZE; c++){
            bool find = false;
            for(int d=0; d<PUZZLE_SIZE; d++){
                if(VarNums[r][c][d] == PRESET_TRUE){
                    find = true;
                    puzzle[r][c] = d + 1;
                }
            }
            if(c < PUZZLE_SIZE-1){
                output_file << puzzle[r][c] << " ";
                cout << puzzle[r][c] << " ";
            }else{
                output_file << puzzle[r][c];
                cout << puzzle[r][c];
            }
        }
        output_file << endl;
        //cout << endl;
    }
    output_file.close();

    std::cin.get();
    return 0;
};



